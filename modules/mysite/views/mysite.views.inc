<?php

/**
 * @file
 * Views handlers for mysite.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function mysite_views_data_alter(&$data) {
  // Add a dummy field on field_data_field_bio that will
  // return the path to the node if the bio field is not empty.
  $data['field_data_field_bio']['field_bio_link'] = array(
    'group' => t('Content'),
    'title' => t('Bio link'),
    'help' => t('Return a link path if the bio is not empty'),
    'real field' => 'field_bio_value',
    'field' => array(
      'handler' => 'mysite_handler_field_bio_link',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Return a path if the bio is not empty.
 *
 * @ingroup views_filter_handlers
 */
class mysite_handler_field_bio_link extends views_handler_field {
  /**
  * Render the field.
  */
  public function render($values) {
    $value = $this->get_value($values);
    return !empty($value) ? url('node/' . $values->nid, array('absolute' => TRUE)) : NULL;
  }
}
