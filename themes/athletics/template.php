<?php

/**
 * @file
 * This file provides theme override functions for the Athletics theme.
 */

/**
 * Implements hook_template_preprocess_field().
 */
function athletics_preprocess_field(&$variables) {
  if ($variables['element']['#field_name'] == 'field_staff_photo') {
    $variables['classes_array'][] = 'photo-frame';
    $variables['classes_array'][] = 'floatleft';
  }
}
